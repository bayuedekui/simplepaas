package com.bayuedekui.paasbackend.utils;

import org.apache.http.client.HttpClient;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.apache.http.ssl.SSLContexts;
import org.apache.http.ssl.TrustStrategy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;

import javax.net.ssl.SSLContext;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.X509Certificate;

/**
 * 绕过客户端认证的工具类
 */
public class RestTemplateUtil {
    private static final Logger logger = LoggerFactory.getLogger(RestTemplate.class);

    private static final RestTemplate restTemplate = restTemplate();

    public static RestTemplate getRestTemplate() {
        return restTemplate;
    }

    private static RestTemplate restTemplate() {
        try {
            TrustStrategy acceptingTrustStrategy = (X509Certificate[] chain, String authType) -> true;
            SSLContext sslContext = SSLContexts.custom().loadTrustMaterial(null, acceptingTrustStrategy).build();
            //连接池最大连接数和每个主机的并发
            PoolingHttpClientConnectionManager poolingHttpClientConnectionManager = new PoolingHttpClientConnectionManager();
//            poolingHttpClientConnectionManager.setMaxTotal(2000);
//            poolingHttpClientConnectionManager.setDefaultMaxPerRoute(200);

            CloseableHttpClient closeableHttpClient = HttpClients.custom().setSSLContext(sslContext).setSSLHostnameVerifier((hostname,sslSession)->true)
                    //.setConnectionManager(poolingHttpClientConnectionManager)
                    .build();

            HttpComponentsClientHttpRequestFactory requestFactory = new HttpComponentsClientHttpRequestFactory(closeableHttpClient);

            //设置超时连接一分钟
            requestFactory.setConnectTimeout(60000);
            RestTemplate restTemplateInstance = new RestTemplate(requestFactory);
//            restTemplateInstance.setErrorHandler(new RestTemplateCustomResponseErrorHandler());

            return restTemplateInstance;

        } catch (NoSuchAlgorithmException | KeyManagementException |KeyStoreException e) {
            logger.error(e.getMessage(),e);
            throw new RuntimeException(e);
        }

    }
}
