package com.bayuedekui.paasbackend.dao;

import com.bayuedekui.paasbackend.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserRepository extends JpaRepository<User,String> {

    /**
     * 查询所有用户
     * @return
     */
    @Override
    List<User> findAll();

    /**
     * 根据用户名查询用户
     *
     * @param name
     * @return
     */
    User findByName(String name);
    
            
            

    
    
}
