package com.bayuedekui.paasbackend.controller;

import com.bayuedekui.paasbackend.utils.RestTemplateUtil;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RestController
public class Test {

    @GetMapping("/info")
    @ResponseBody
    public String getDockerInfo(){
        RestTemplate rest = new RestTemplate();
        HttpHeaders headers=new HttpHeaders();
        headers.add("Authorization","Bearer 7db2f1c02d721320");
        ResponseEntity<String> response = rest.exchange("https://192.168.172.135:6443/api/v1/namespaces/default/replicationcontrollers",
                HttpMethod.GET, new HttpEntity<>(headers), String.class);



//        ResponseEntity<String> response = RestTemplateUtil.getRestTemplate().exchange("https://192.168.172.135:6443/api/v1/namespaces/default/replicationcontrollers",
//                HttpMethod.GET, new HttpEntity<>(null,headers), String.class);
        System.out.println(response.getBody().toString());
        return  response.getBody().toString();

    }


}
