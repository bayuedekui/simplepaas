package com.bayuedekui.paasbackend.model;

import lombok.Data;

@Data
public class K8sBase {
    /**
     * Kubernetes的api版本号，目前是v1.0
     */
    private String apiVersion="v1";

    /**
     * 创建的对象类型，本项目中主要用到Service和ReplicationController
     */
    private String kind;

    /**
     * 创建对象的元数据信息
     */
    private MetaData metadata;
}
