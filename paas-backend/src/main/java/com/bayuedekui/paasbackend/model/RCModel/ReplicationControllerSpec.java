package com.bayuedekui.paasbackend.model.RCModel;

import com.bayuedekui.paasbackend.model.Selector;
import lombok.Data;

@Data
public class ReplicationControllerSpec {
    /**
     * pod副本数
     */
    private int replicas;

    /**
     * Pod选择器
     */
    private Selector selector;


    /**
     * RC的定义Pod的模板
     */
    private Template template;


}
