package com.bayuedekui.paasbackend.model.RCModel;

import lombok.Data;

@Data
public class ContainerPort {
    private int containerPort;
}
