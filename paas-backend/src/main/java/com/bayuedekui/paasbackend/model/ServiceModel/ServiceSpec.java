package com.bayuedekui.paasbackend.model.ServiceModel;

import com.bayuedekui.paasbackend.model.Selector;
import lombok.Data;

import java.util.List;

@Data
public class ServiceSpec {

    /**
     * 类型，向外暴露的端口类型，内部域名还是node节点域名
     */
    private String type;

    /**
     * service的pod类型
     */
    private List<ServicePorts> ports;

    /**
     * selector与rc对应起来
     */
    private Selector selector;

}
