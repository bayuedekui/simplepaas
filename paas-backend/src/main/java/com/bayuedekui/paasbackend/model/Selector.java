package com.bayuedekui.paasbackend.model;

import lombok.Data;

@Data
public class Selector {
    private String name;
}
