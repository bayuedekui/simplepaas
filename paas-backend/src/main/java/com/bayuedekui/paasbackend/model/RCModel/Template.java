package com.bayuedekui.paasbackend.model.RCModel;

import com.bayuedekui.paasbackend.model.RCModel.PodSpec;
import com.bayuedekui.paasbackend.model.RCModel.RCSpecMetaData;
import lombok.Data;

@Data
public class Template {
    /**
     * 模板的基本信息
     */
    private RCSpecMetaData metadata;

    /**
     * 模板的详细信息
     */
    private PodSpec spec;

}
