package com.bayuedekui.paasbackend.model.RCModel;

import com.bayuedekui.paasbackend.model.K8sBase;
import lombok.Data;

@Data
public class ReplicationController extends K8sBase {
    /**
     * RC的spec
     */
    private ReplicationControllerSpec spec;
}
