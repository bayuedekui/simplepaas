package com.bayuedekui.paasbackend.model;

import com.bayuedekui.paasbackend.model.RCModel.Labels;
import lombok.Data;

@Data
public class MetaData {
    private String name;

    private Labels labels;
}
