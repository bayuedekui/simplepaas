package com.bayuedekui.paasbackend.model.RCModel;

import com.bayuedekui.paasbackend.model.RCModel.Labels;
import lombok.Data;

@Data
public class RCSpecMetaData {
    /**
     * RCSpec中的metadata，没有name属性，只有label属性
     */
    private Labels labels;
}
