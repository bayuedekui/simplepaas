package com.bayuedekui.paasbackend.model.ServiceModel;

import com.bayuedekui.paasbackend.model.K8sBase;
import lombok.Data;

@Data
public class Service extends K8sBase {

    /*
        service的spec
     */
    private ServiceSpec spec;
}
