package com.bayuedekui.paasbackend.model.RCModel;

import com.bayuedekui.paasbackend.model.RCModel.Container;
import lombok.Data;

import java.util.List;

@Data
public class PodSpec {

    /**
     * Pod中的容器列表，至少一个容器，同时Pod中会自动创建一个pause容器，来创建共享协议栈等
     */
    private List<Container> containers;
}
