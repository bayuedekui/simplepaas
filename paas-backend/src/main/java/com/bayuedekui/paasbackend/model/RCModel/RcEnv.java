package com.bayuedekui.paasbackend.model.RCModel;

import lombok.Data;

@Data
public class RcEnv {
    /**
     * 设置env的属性名称
     */
    private String name;

    /**
     * env属性值
     */
    private String value;

}
