package com.bayuedekui.paasbackend.model.RCModel;

import lombok.Data;

import java.util.List;

@Data
public class Container {

    /**
     * 容器名称
     */
    private String name;

    /**
     * 容器的镜像名称
     */
    private String image;

    /**
     * 镜像的pull策略
     */
    private String imagePullPolicy;

    /**
     * 容器的端口号配置
     */
    private List<ContainerPort> ports;

    /**
     * rc服务的密码设置
     */
    private List<RcEnv> env;
}
