package com.bayuedekui.paasbackend.model.ServiceModel;

import com.bayuedekui.paasbackend.model.Selector;
import lombok.Data;

@Data
public class ServicePorts {

    /**
     * 服务端口号
     */
    private int port;

    /**
     * 协议
     */
    private String protocol;

    /**
     * 名称
     */
    private String name;

    /**
     * 目标端口
     */
    private String targetPort;

    /**
     * node节点暴露的端口，范围在（30000-32767）
     */
    private int nodePort;


}
